import React, { useMemo } from "react";
import { useDispatch } from "react-redux";
import { useAppDispatch, useAppSelector } from "soapbox/hooks";
import { useHistory } from "react-router-dom";

interface AppIconProps {
  name: string;
  color: string;
  onClickIcon: (value: string) => void;
}

const AppIcon: React.FC<AppIconProps> = ({ name, color, onClickIcon }) => {
  const dispatch = useAppDispatch();
  const history = useHistory();

  const handleClick = () => {
    onClickIcon(name);
    history.push("/");
  };

  return (
    <div
      onClick={handleClick}
      className="cursor-pointer"
      style={{
        width: "150px",
        height: "100px",
        minWidth: "150px",
        padding: "5px",
        backgroundColor: color,
        borderRadius: 10,
        boxShadow: "0px 0px 10px rgba(0,0,0,0.5)",
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-evenly",
        alignItems: "center",
        marginInline: "20px",
      }}
    >
      <div className="flex justify-center items-center w-full">
          <span className="font-semibold">{name}</span>
        </div>
    </div>
  );
};

interface IExperienceSelector {
  value: string;
  onChange: (value: string) => void;
}

const ExperienceSelector: React.FC<IExperienceSelector> = ({
  value,
  onChange,
}) => {

  const handleChange = (app: string) => {
    onChange(app);
  };

  const rows = [
    {
      title: "Algorithms",
      appIcons: [
        { name: "Chronological", color: "lightgreen" },
        { name: "Hot Ranking", color: "grey" },
        // { name: 'App4', color: 'lightsalmon' },
      ],
    },
    {
      title: "Experiences",
      appIcons: [
        { name: "Traditional", color: "lightblue" },
        { name: "Graph", color: "lightpink" },
      ],
    },
  ];
  return (
    <div>
      {rows.map((row, rowno) => (
        <div style={{ display: "flex", flexDirection: 'column'}}>
        <p className="p-5 font-bold">{row.title}</p>
        <div key={row.title} style={{ display: "flex", marginBottom: "100px" }}>
          {row.appIcons.map((item, itemno) => {
            const appIconIndex = rowno * 4 + itemno;
            const appIcon = item;
            return (
              <AppIcon
                key={appIconIndex}
                name={appIcon.name}
                color={appIcon.color}
                onClickIcon={handleChange}
              />
            );
          })}
        </div>
      </div>
      ))}
    </div>
  );
};

export default ExperienceSelector;
