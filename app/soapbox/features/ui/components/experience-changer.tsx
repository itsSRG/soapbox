import React from "react";

import { changeSetting } from "soapbox/actions/settings";
import { useAppDispatch, useSettings } from "soapbox/hooks";

import ExperienceSelector from "./experience-selector";

/** Stateful theme selector. */
const ExperienceChanger: React.FC = () => {
  const dispatch = useAppDispatch();
  const experienceMode = useSettings().get("experience");
  const handleChange = (experienceMode: string) => {
    dispatch(changeSetting(["experience"], experienceMode));
  };

  return (
    <>
      <ExperienceSelector value={experienceMode} onChange={handleChange} />
    </>
  );
};

export default ExperienceChanger;
