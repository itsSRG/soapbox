import { OrderedSet as ImmutableOrderedSet } from 'immutable';
import debounce from 'lodash/debounce';
import React, { useCallback, useEffect, useState } from 'react';
import { defineMessages } from 'react-intl';

import { dequeueTimeline, scrollTopTimeline } from 'soapbox/actions/timelines';
import ScrollTopButton from 'soapbox/components/scroll-top-button';
import StatusList, { IStatusList } from 'soapbox/components/status-list';
import { useAppSelector, useAppDispatch, useSettings, useSoapboxConfig } from 'soapbox/hooks';
import { makeGetStatus, makeGetStatusIds } from 'soapbox/selectors';

import { getReactForStatus, reduceEmoji } from 'soapbox/utils/emoji-reacts';
import { List as ImmutableList } from 'immutable';
import {Status} from "soapbox/types/entities"

const messages = defineMessages({
  queue: { id: 'status_list.queue_label', defaultMessage: 'Click to see {count} new {count, plural, one {post} other {posts}}' },
});

interface ITimeline extends Omit<IStatusList, 'statusIds' | 'isLoading' | 'hasMore'> {
  /** ID of the timeline in Redux. */
  timelineId: string
  /** Settings path to use instead of the timelineId. */
  prefix?: string
}

/** Scrollable list of statuses from a timeline in the Redux store. */
const Timeline: React.FC<ITimeline> = ({
  timelineId,
  onLoadMore,
  prefix,
  ...rest
}) => {
  const dispatch = useAppDispatch();
  const getStatusIds = useCallback(makeGetStatusIds(), []);
  const settings = useSettings();

  const lastStatusId = useAppSelector(state => (state.timelines.get(timelineId)?.items || ImmutableOrderedSet()).last() as string | undefined);
  const statusIds = useAppSelector(state => getStatusIds(state, { type: timelineId, prefix }));    
  const getStatus = makeGetStatus();
  const statuses: Array<[string, Status | null]> = useAppSelector(state => Array.from(statusIds).map(id => [id, getStatus(state, { id: id})] ))
  const isLoading = useAppSelector(state => (state.timelines.get(timelineId) || { isLoading: true }).isLoading === true);
  const isPartial = useAppSelector(state => (state.timelines.get(timelineId)?.isPartial || false) === true);
  const hasMore = useAppSelector(state => state.timelines.get(timelineId)?.hasMore === true);
  const totalQueuedItemsCount = useAppSelector(state => state.timelines.get(timelineId)?.totalQueuedItemsCount || 0);
  const isFilteringFeed = useAppSelector(state => !!state.timelines.get(timelineId)?.feedAccountId);
  const experienceMode = useSettings().get('experience');
  const soapboxConfig = useSoapboxConfig();

  var idScore: [string, number][] = []

  const handleDequeueTimeline = () => {
    if (isFilteringFeed) {
      return;
    }

    dispatch(dequeueTimeline(timelineId, onLoadMore));
  };

  const handleScrollToTop = useCallback(debounce(() => {
    dispatch(scrollTopTimeline(timelineId, true));
  }, 100), [timelineId]);

  const handleScroll = useCallback(debounce(() => {
    dispatch(scrollTopTimeline(timelineId, false));
  }, 100), [timelineId]);

  useEffect(() => {
    if(experienceMode == 'Hot Ranking')
    {
      if(onLoadMore && lastStatusId && statusIds.size < 100)
      onLoadMore(lastStatusId);
    }
    
  }, [lastStatusId]);

  const epoch: Date = new Date(1970, 0, 1);

  function epochSeconds(date: Date): number {
    const td: number = (date.getTime() - epoch.getTime()) / 1000;
    return td;
  }

  function hot(score: number, date: Date): number {
    const s: number = score;
    const order: number = Math.log10(Math.max(Math.abs(s), 1));
    const sign: number = s > 0 ? 1 : s < 0 ? -1 : 0;
    const seconds: number = epochSeconds(date) - 1134028003;
    return Math.round((sign * order + seconds / 45000) * 10000000) / 10000000;
  }

  if( experienceMode == 'Hot Ranking' )
  {
    if(statuses)
    {
    for (const status of statuses)
    {
      if(status[1])
      {
      const favouriteCount = status[1].favourites_count;
      const { allowedEmoji } = soapboxConfig;
      const emojiReactCount = reduceEmoji(
        (status[1].pleroma.get('emoji_reactions') || ImmutableList()) as ImmutableList<any>,
        favouriteCount,
        status[1].favourited,
        allowedEmoji,
      ).reduce((acc, cur) => acc + cur.get('count'), 0);
      const date = new Date(status[1].created_at)
      idScore.push([status[0], hot(emojiReactCount, date)]);
    }
  } 
  }
  const sortedKeys = idScore.sort((a,b) => b[1] - a[1]).map(([key]) => key);
    return (
      <>
      <StatusList
        timelineId={timelineId}
        onScrollToTop={handleScrollToTop}
        onScroll={handleScroll}
        lastStatusId={lastStatusId}
        statusIds={ImmutableOrderedSet(sortedKeys)}
        isLoading={isLoading}
        isPartial={isPartial}
        hasMore={hasMore}
        onLoadMore={onLoadMore}
        {...rest}
      />
      </>
    );
  }

  return (
    <>
      <ScrollTopButton
        key='timeline-queue-button-header'
        onClick={handleDequeueTimeline}
        count={totalQueuedItemsCount}
        message={messages.queue}
      />

      <StatusList
        timelineId={timelineId}
        onScrollToTop={handleScrollToTop}
        onScroll={handleScroll}
        lastStatusId={lastStatusId}
        statusIds={statusIds}
        isLoading={isLoading}
        isPartial={isPartial}
        hasMore={hasMore}
        onLoadMore={onLoadMore}
        {...rest}
      />
    </>
  );
};

export default Timeline;
