// import React from "react";

// const Card = ({ name, title, imageUrl }) => {
//   return (
//     <div className="card">
//       <img className="profile-img" src={imageUrl} alt="Profile" />
//       <div className="text-container">
//         <h2 className="name">{name}</h2>
//         <p className="title">{title}</p>
//       </div>
//     </div>
//   );
// };

// export default Card;

// import React from 'react';

// const Card = ({ text, imageUrl, name, username }) => {
//   return (
//     <div
//       style={{
//         display: 'flex',
//         alignItems: 'center',
//         padding: '16px',
//         boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.1)',
//         borderRadius: '8px',
//       }}
//     >
//       <img
//         src={imageUrl}
//         alt={name}
//         style={{ width: '48px', height: '48px', marginRight: '16px', borderRadius: '50%' }}
//       />
//       <div>
//         <h2 style={{ fontWeight: 'bold', fontSize: '18px', marginBottom: '4px' }}>{name}</h2>
//         <p style={{ fontSize: '14px', color: '#657786', marginBottom: '4px' }}>@{username}</p>
//         <p style={{ fontSize: '16px', lineHeight: '24px', marginBottom: 0 }}>{text}</p>
//       </div>
//     </div>
//   );
// };

// export default Card;

// import { useState } from "react";

// const Card = ({ text, imageUrl, isReal }) => {
//   const [isHovering, setIsHovering] = useState(false);

//   const handleMouseEnter = () => {
//     setIsHovering(true);
//   };

//   const handleMouseLeave = () => {
//     setIsHovering(false);
//   };

//   return (
//     <div
//       onMouseEnter={handleMouseEnter}
//       onMouseLeave={handleMouseLeave}
//       style={{ maxWidth: "400px", margin: "0 auto", position: "relative" }}
//     >
//       <img src={imageUrl} alt="" style={{ width: "100%" }} />
//       {isHovering && (
//         <div
//           style={{
//             position: "absolute",
//             top: "0",
//             left: "0",
//             height: "100%",
//             width: "100%",
//             display: "flex",
//             justifyContent: "center",
//             alignItems: "center",
//             background: "rgba(0, 0, 0, 0.5)",
//             color: "white",
//             fontSize: "24px",
//             fontWeight: "bold"
//           }}
//         >
//           {isReal ? (
//             <span role="img" aria-label="Real">
//               🟢
//             </span>
//           ) : (
//             <span role="img" aria-label="Fake">
//               🔴
//             </span>
//           )}
//         </div>
//       )}
//       <div style={{ padding: "16px" }}>
//         <p style={{ fontWeight: "bold", marginBottom: "8px" }}>{text}</p>
//       </div>
//     </div>
//   );
// };

// export default Card;

import React, { useState } from "react";

// type TweetProps = {
//   text: string;
//   imageUrl: string;
//   name: string;
//   username: string;
//   predVal: number;
// };

// function Card({text}:{text:any}, {imageUrl}:{imageUrl:any},{name}:{name:any},{username}:{username:any},{predVal}:{predVal:any}) {


function Card({ text, imageUrl, name, username, predVal}:any) {
  const [isHovered, setIsHovered] = useState(false);

  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        padding: "16px",
        boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.1)",
        borderRadius: "8px",
        position: "relative"
      }}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      {isHovered && (
        <div
          style={{
            position: "absolute",
            top: "-1px",
            right: "-1px",
            width: "20px",
            height: "20px",
            borderRadius: "50%",
            backgroundColor: predVal ? "red" : "green",
            zIndex: 1
          }} />
      )}
      <img
        src={imageUrl}
        alt={name}
        style={{
          width: "48px",
          height: "48px",
          marginRight: "16px",
          borderRadius: "50%"
        }} />
      <div>
        <h2
          style={{ fontWeight: "bold", fontSize: "18px", marginBottom: "4px" }}
        >
          {name}
        </h2>
        <p style={{ fontSize: "14px", color: "#657786", marginBottom: "4px" }}>
          @{username}
        </p>
        <p style={{ fontSize: "16px", lineHeight: "24px", marginBottom: 0 }}>
          {text}
        </p>
      </div>
    </div>
  );
}

export default Card;
