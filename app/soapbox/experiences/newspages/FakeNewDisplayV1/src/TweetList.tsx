import React from "react";
import tweet from "./data.json";
import Card from "./Card";

const TweetList = () => {
  return (
    <div>
      {tweet.map((tweet) => (
        <Card
          text={tweet.text}
          imageUrl={tweet.Image_url}
          name={tweet.screenname}
          username={tweet.screenname}
          predVal={tweet.Prid_Val}
        />
      ))}
    </div>
  );
};

export default TweetList;
