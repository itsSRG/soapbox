import React from "react";
import { useAppDispatch, useAppSelector, useTheme } from "soapbox/hooks";
import { makeGetAccount } from "soapbox/selectors";
import { useHistory } from "react-router-dom";
import { AppDispatch, RootState } from "soapbox/store";
import CytoscapeComponent from "react-cytoscapejs";
import type { Account } from "soapbox/types/entities";
import { OrderedSet as ImmutableOrderedSet } from "immutable";

function redirectToAccount(accountId: string, routerHistory: any) {
  return (_dispatch: AppDispatch, getState: () => RootState) => {
    const acct = getState().getIn(["accounts", accountId, "acct"]);

    if (acct && routerHistory) {
      routerHistory.push(`/@${acct}`);
    }
  };
}

export const DisplayGraph = ({
  accountIds,
  viewer,
}: {
  accountIds: ImmutableOrderedSet<string>;
  viewer: Account;
}) => {
  const history = useHistory();
  const dispatch = useAppDispatch();
  const getAccount = React.useCallback(makeGetAccount(), []);
  const theme = useTheme();
  const accounts = useAppSelector((state) =>
    Array.from(accountIds).map((id) => getAccount(state, id))
  );

  const elements: [
    | {
        data: { id: string; label: string };
        position: { x: number; y: number };
      }
    | { data: { id: string; label: string } }
  ] = [
    {
      data: {
        id: "You Do Not Follow Anyone Yet !",
        label: "You Do Not Follow Anyone Yet !",
      },
    },
  ];

  const cytoscapeStylesheet: Array<cytoscape.Stylesheet> = [];

  accounts.forEach((account) => {
    if (account && account.id && account.display_name) {
      elements.push({
        data: {
          id: account?.id,
          label: account.display_name.replace(":verified:", ""),
        },
      });
      cytoscapeStylesheet.push(
        {
          selector: "node",
          style: {
            height: 80,
            width: 80,
            "background-fit": "cover",
            "border-color": "#000",
            "border-width": 3,
            "border-opacity": 0.5,
          },
        },
        {
          selector: "node[label]",
          style: {
            label: "data(label)",
            "font-size": "18",
            color: theme == "dark" ? "#ffffff" : "#000000",
          },
        },
        {
          selector: "node.highlight",
          style: {
            "border-color": "#FFF",
            "border-width": "2px",
          },
        },
        {
          selector: "node[label].highlight",
          style: {
            label: "data(label)",
            "font-size": "24",
            "font-weight": "bold",
          },
        },
        {
          selector: "#" + account.id,
          style: {
            "background-image": account.avatar,
          },
        }
      );
    }
  });
  if (elements.length > 1) elements.splice(0, 1);

  var idOrderMap: { [key: string]: number } = {};

  var currentCircle: number = 5;
  var order: number =
    elements.length - 1 > currentCircle ? 5 : elements.length - 1;
  var distance: number = 100;
  elements.forEach((element, i) => {
    idOrderMap[element.data.id] = -order;
    if (i == currentCircle + 1) {
      distance *= 2;
      order = elements.length - 1 - currentCircle;
      order = order > currentCircle * 2 ? currentCircle * 2 : order;
      currentCircle = currentCircle + currentCircle * 2;
    }
  });

  let options = {
    name: "concentric",

    fit: true, // whether to fit the viewport to the graph
    padding: 30, // the padding on fit
    startAngle: (3 / 2) * Math.PI, // where nodes start in radians
    sweep: undefined, // how many radians should be between the first and last node (defaults to full circle)
    clockwise: true, // whether the layout should go clockwise (true) or counterclockwise/anticlockwise (false)
    equidistant: false, // whether levels have an equal radial distance betwen them, may cause bounding box overflow
    minNodeSpacing: 80, // min spacing between outside of nodes (used for radius adjustment)
    boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
    avoidOverlap: true, // prevents node overlap, may overflow boundingBox if not enough space
    nodeDimensionsIncludeLabels: false, // Excludes the label when calculating node bounding boxes for the layout algorithm
    height: undefined, // height of layout area (overrides container height)
    width: undefined, // width of layout area (overrides container width)
    spacingFactor: undefined, // Applies a multiplicative factor (>0) to expand or compress the overall area that the nodes take up
    concentric: function (node: any) {
      // returns numeric value for each node, placing higher nodes in levels towards the centre
      return idOrderMap[node.id()];
    },
    levelWidth: function (nodes: any) {
      // the variation of concentric values in each level
      return 2;
    },
    animate: false, // whether to transition the node positions
    animationDuration: 500, // duration of animation in ms if enabled
    animationEasing: undefined, // easing of animation if enabled
    animateFilter: function (node: any, i: any) {
      return true;
    }, // a function that determines whether the node should be animated.  All nodes animated by default on animate enabled.  Non-animated nodes are positioned immediately when the layout starts
    ready: undefined, // callback on layoutready
    stop: undefined, // callback on layoutstop
    transform: function (node: any, position: any) {
      return position;
    }, // transform a given node position. Useful for changing flow direction in discrete layouts
  };

  return (
    <div id="cy-box" className="h-screen">
      <CytoscapeComponent
        cy={(cy) => {
          cy.fit();
          cy.on("mouseover", "node", function (e) {
            var sel = e.target;
            cy.elements()
              .difference(sel.outgoers())
              .not(sel)
              .addClass("semitransp");
            sel.addClass("highlight").outgoers().addClass("highlight");
          });

          cy.on("mouseout", "node", function (e) {
            var sel = e.target;
            cy.elements().removeClass("semitransp");
            sel.removeClass("highlight").outgoers().removeClass("highlight");
          });

          cy.on("tap", "node", function (e) {
            if (!cy.data("tapListenerAdded")) {
              cy.data("tapListenerAdded", true);
              const node = e.target;
              const currentPath = window.location.pathname;
              dispatch(redirectToAccount(node.id(), history));
            }
          });
        }}
        elements={elements}
        stylesheet={cytoscapeStylesheet}
        layout={options}
        style={{ width: "100%", height: "100%" }}
        zoomingEnabled={true}
        userZoomingEnabled={true}
        minZoom={0.1}
        maxZoom={10}
        wheelSensitivity={0.1}
      />
      <style>
        {`
    #cy-box {
      position: relative;
    }
    #legend {
      position: absolute;
      color: black;
      top: 10px;
      left: 10px;
      background-color: white;
      padding: 10px;
      border: 1px solid black;
      z-index: 10;
    }
    
    .legend-color {
      display: inline-block;
      width: 10px;
      height: 10px;
      margin-right: 5px;
    }
  `}
      </style>
    </div>
  );
};
