import { OrderedSet as ImmutableOrderedSet } from "immutable";
import React, { useEffect, useState } from "react";
import { FormattedMessage, useIntl } from "react-intl";

import {
  fetchAccount,
  fetchFollowing,
  fetchAccountByUsername,
} from "soapbox/actions/accounts";
import MissingIndicator from "soapbox/components/missing-indicator";
import { Layout, Spinner } from "soapbox/components/ui";
import {
  useAppDispatch,
  useAppSelector,
  useFeatures,
  useOwnAccount,
} from "soapbox/hooks";

import { DisplayGraph } from "soapbox/experiences/graph/displayGraph";

interface IFollowing {
  params?: {
    username?: string;
  };
}

/** Displays a list of accounts the given user is following. */
const GraphHomePage: React.FC<IFollowing> = (props) => {
  const id = useAppSelector((state) => state.me);
  const account = useOwnAccount();

  const intl = useIntl();
  const dispatch = useAppDispatch();
  const features = useFeatures();
  const ownAccount = useOwnAccount();

  const [loading, setLoading] = useState(true);

  const username = props.params?.username || "";
  const isOwnAccount =
    username.toLowerCase() === ownAccount?.username?.toLowerCase();

  const accountIds = useAppSelector(
    (state) =>
      state.user_lists.following.get(account!?.id)?.items ||
      ImmutableOrderedSet<string>()
  );
  const isUnavailable = useAppSelector((state) => {
    const blockedBy =
      state.relationships.getIn([account?.id, "blocked_by"]) === true;
    return isOwnAccount ? false : blockedBy && !features.blockersVisible;
  });

  useEffect(() => {
    let promises = [];

    if (account) {
      promises = [
        dispatch(fetchAccount(account.id)),
        dispatch(fetchFollowing(account.id)),
      ];
    } else {
      promises = [dispatch(fetchAccountByUsername(username))];
    }

    Promise.all(promises)
      .then(() => setLoading(false))
      .catch(() => setLoading(false));
  }, [account?.id, username]);

  if (loading && accountIds.isEmpty()) {
    return <Spinner />;
  }

  if (!account) {
    return <MissingIndicator />;
  }

  if (isUnavailable) {
    return (
      <div className="empty-column-indicator">
        <FormattedMessage
          id="empty_column.account_unavailable"
          defaultMessage="Profile unavailable"
        />
      </div>
    );
  }

  return (
    <>
      <Layout.Main className="md:col-span-12 lg:col-span-12 xl:col-span-12 pb-36">
        <DisplayGraph accountIds={accountIds} viewer={account} />
      </Layout.Main>
    </>
  );
};

export default GraphHomePage;
