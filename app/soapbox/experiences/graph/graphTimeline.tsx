import React, { useEffect, useState, useCallback } from "react";
import { FormattedMessage } from "react-intl";
import { useHistory } from "react-router-dom";

import { fetchAccountByUsername } from "soapbox/actions/accounts";
import { fetchPatronAccount } from "soapbox/actions/patron";
import {
  expandAccountFeaturedTimeline,
  expandAccountTimeline,
} from "soapbox/actions/timelines";
import MissingIndicator from "soapbox/components/missing-indicator";
import { Card, CardBody, Spinner, Text } from "soapbox/components/ui";
import {
  useAppDispatch,
  useAppSelector,
  useFeatures,
  useSettings,
  useSoapboxConfig,
} from "soapbox/hooks";
import {
  makeGetStatus,
  makeGetStatusIds,
  findAccountByUsername,
} from "soapbox/selectors";

import GraphExperience from "./graph";

const getStatusIds = makeGetStatusIds();
const getStatus = makeGetStatus();
interface IAccountTimeline {
  params: {
    username: string;
  };
  withReplies?: boolean;
}

const GraphTimeline: React.FC<IAccountTimeline> = ({
  params,
  withReplies = false,
}) => {
  const history = useHistory();
  const dispatch = useAppDispatch();
  const features = useFeatures();
  const settings = useSettings();
  const soapboxConfig = useSoapboxConfig();

  const account = useAppSelector((state) =>
    findAccountByUsername(state, params.username)
  );
  const [accountLoading, setAccountLoading] = useState<boolean>(!account);

  const path = withReplies ? `${account?.id}:with_replies` : account?.id;
  const statusIds = useAppSelector((state) =>
    getStatusIds(state, { type: `account:${path}`, prefix: "account_timeline" })
  );
  const idContentMap: { [key: string]: string } = useAppSelector((state) =>
    Array.from(statusIds)
      .flatMap((id) => {
        const status = getStatus(state, { id: id });
        if (status) return [{ [id]: status.content }];
        else return [];
      })
      .reduce((acc, curr) => ({ ...acc, ...curr }), {})
  );

  const isBlocked = useAppSelector(
    (state) => state.relationships.getIn([account?.id, "blocked_by"]) === true
  );
  const unavailable = isBlocked && !features.blockersVisible;
  const patronEnabled =
    soapboxConfig.getIn(["extensions", "patron", "enabled"]) === true;
  const accountUsername = account?.username || params.username;

  useEffect(() => {
    dispatch(fetchAccountByUsername(params.username, history))
      .then(() => setAccountLoading(false))
      .catch(() => setAccountLoading(false));
  }, [params.username]);

  useEffect(() => {
    if (account && !withReplies) {
      dispatch(expandAccountFeaturedTimeline(account.id));
    }
  }, [account?.id, withReplies]);

  useEffect(() => {
    if (account && patronEnabled) {
      dispatch(fetchPatronAccount(account.url));
    }
  }, [account?.url, patronEnabled]);

  useEffect(() => {
    if (account) {
      dispatch(expandAccountTimeline(account.id, { withReplies }));
    }
  }, [account?.id, withReplies]);

  const handleLoadMore = (maxId: string) => {
    if (account) {
      dispatch(expandAccountTimeline(account.id, { maxId, withReplies }));
    }
  };

  if (!account && accountLoading) {
    return <Spinner />;
  } else if (!account) {
    return <MissingIndicator nested />;
  }

  if (unavailable) {
    return (
      <Card>
        <CardBody>
          <Text align="center">
            {isBlocked ? (
              <FormattedMessage
                id="empty_column.account_blocked"
                defaultMessage="You are blocked by @{accountUsername}."
                values={{ accountUsername }}
              />
            ) : (
              <FormattedMessage
                id="empty_column.account_unavailable"
                defaultMessage="Profile unavailable"
              />
            )}
          </Text>
        </CardBody>
      </Card>
    );
  }
  return (
    <GraphExperience postAndContent={idContentMap} accountName={account.acct} />
  );
};

export default GraphTimeline;
