import React, { useEffect, useState, useRef } from "react";
import CytoscapeComponent from "react-cytoscapejs";
import { useHistory } from "react-router-dom";
import { useAppDispatch, useTheme } from "soapbox/hooks";
import { AppDispatch, RootState } from "soapbox/store";
import { Button, Spinner } from "soapbox/components/ui";

import Cytoscape, { Core } from "cytoscape";
const cise = require("cytoscape-cise");

Cytoscape.use(cise);

function redirectToPost(
  accountName: string,
  postId: string,
  routerHistory: any
) {
  return (_dispatch: AppDispatch, getState: () => RootState) => {
    // const acct = getState().getIn(['accounts', postId, 'acct']);

    if (accountName && routerHistory) {
      routerHistory.push(`/@${accountName}/posts/${postId}`);
    }
  };
}

const MyApp = ({
  postAndContent,
  accountName,
}: {
  postAndContent: { [key: string]: string };
  accountName: string;
}) => {
  const history = useHistory();
  const dispatch = useAppDispatch();
  const cyRef = useRef<Core | null>(null);
  const containerRef = useRef<HTMLDivElement>(null);
  const theme = useTheme();
  const [elements, setElements] = useState<
    [
      | {
          data: { id: string; label: string };
          position: { x: number; y: number };
        }
      | { data: { id: string; label: string } }
      | { data: { source: string; target: string } }
    ]
  >([{ data: { id: "You", label: "You" } }]);

  const [layout, setLayout] = useState<{
    name: string;
    clusters: Array<Array<string>>;
    boundingBox: { x1: number; y1: number; w: number; h: number };
  }>({
    name: "cise",
    clusters: [],
    boundingBox: { x1: 0, y1: 0, w: 600, h: 600 },
  });

  const [loading, setLoading] = useState(true);

  function trimString(str: string, maxLength: number) {
    if (str.length > maxLength) {
      return str.slice(0, maxLength) + "...";
    } else {
      return str;
    }
  }

  const handlePanToOrigin = () => {
    if (cyRef.current) {
      cyRef.current.fit();
      cyRef.current.center();
    }
  };

  useEffect(() => {
    if (containerRef.current) {
      const { width, height } = containerRef.current.getBoundingClientRect();
      var cleanIdAndContent: { [key: string]: string } = {};

      Object.keys(postAndContent).forEach((key) => {
        const temp = postAndContent[key]
          .replace(/(<([^>]+)>)/gi, "")
          .replace("&#39;", "'");
        if (temp) {
          cleanIdAndContent[key] = temp;
        }
      });

      if (Object.keys(cleanIdAndContent).length !== 0) {
        fetch("https://graph.vikalp.social", {
          method: "POST",
          body: JSON.stringify(cleanIdAndContent),
          headers: {
            "Content-type": "application/json; charset=UTF-8",
          },
        })
          .then((response) => response.json())
          .then((data) => {
            var newelements: [
              | {
                  data: { id: string; label: string };
                  position: { x: number; y: number };
                }
              | { data: { id: string; label: string } }
              | { data: { id: string; label: string }; classes: string }
              | { data: { source: string; target: string } }
            ] = [
              {
                data: {
                  id: "No Posts From The User !",
                  label: "No Posts From The User !",
                },
              },
            ];

            Object.keys(cleanIdAndContent).forEach((id) => {
              newelements.push({
                data: { id: id, label: trimString(cleanIdAndContent[id], 100) },
              });
            });

            let clusterList: Array<Array<string>> = [];
            Object.keys(data).forEach((category) => {
              let cluster: Array<string> = [];
              newelements.push(
                { data: { id: category, label: category }, classes: "category" }
                // { data: { source: "You", target: category } }
              );

              Object.keys(data[category]).forEach((postId) => {
                cluster.push(postId);
                newelements.push({
                  data: { source: category, target: postId },
                });
              });
              clusterList.push(cluster);
            });
            if (newelements.length > 1) {
              newelements.splice(0, 1);
            }
            setElements([...newelements]);
            setLayout({
              name: "cise",
              clusters: clusterList,
              boundingBox: { x1: 0, y1: 0, w: 600, h: 600 },
            });
            setLoading(false);
          })
          .catch((err) => {
            console.log(err.message);
          });
      }
    }
  }, [postAndContent]);

  const cytoscapeStylesheet: Array<cytoscape.Stylesheet> = [
    {
      selector: "node",
      style: {
        "background-color": theme == "dark" ? "#1976d2" : "#9ee1f7",
        width: "label",
        height: "label",
        // a single "padding" is not supported in the types :(
        "padding-top": "8",
        "padding-bottom": "8",
        "padding-left": "8",
        "padding-right": "8",
        // this fixes the text being shifted down on nodes (sadly no fix for edges, but it's not as obvious there without borders)
        "text-margin-y": -3,
        "text-background-padding": "20",
        shape: "round-rectangle",
        // label: 'My multiline\nlabel',
      },
    },
    {
      selector: ".category",
      style: {
        "background-color": theme == "dark" ? "purple" : "#90fc90",
        shape: "ellipse",
        width: function (ele: any) {
          return ele.degree() * 25;
        },
        height: function (ele: any) {
          return ele.degree() * 25;
        },
        "font-size": function (ele: any) {
          return ele.degree() * 25;
        },
      },
    },
    {
      selector: "node[label]",
      style: {
        label: "data(label)",
        "font-size": "24",
        color: theme == "dark" ? "#ffffff" : "#000000",
        "text-halign": "center",
        "text-valign": "center",
        "text-wrap": "wrap",
        "text-max-width": "300",
      },
    },
    {
      selector: "edge",
      style: {
        "curve-style": "bezier",
        "target-arrow-shape": "triangle",
        opacity: 0.4,
        width: 1.5,
      },
    },
    {
      selector: "edge[label]",
      style: {
        label: "data(label)",
        "font-size": "12",
        "text-background-color": "white",
        "text-background-opacity": 1,
        "text-background-padding": "2px",
        "text-margin-y": -4,
        // so the transition is selected when its label/name is selected
        "text-events": "yes",
      },
    },
    {
      selector: "node.highlight",
      style: {
        "border-color": "#FFF",
        "border-width": "2px",
      },
    },
    {
      selector: "node.semitransp",
      style: { opacity: 0.5 },
    },
    {
      selector: "edge.highlight",
      style: { "mid-target-arrow-color": "#FFF" },
    },
    {
      selector: "edge.semitransp",
      style: { opacity: 0.2 },
    },
  ];

  if (loading) {
    return (
      <div ref={containerRef}>
        <Spinner />
      </div>
    );
  }

  return (
    <>
      <div id="cy-box" className="h-screen" ref={containerRef}>
        <CytoscapeComponent
          cy={(cy) => {
            cy.fit();
            cy.center();
            // cy.pan({ x: 300, y: 300 });
            cy.on("tap", "node", function (e) {
              if (!cy.data("tapListenerAdded")) {
                cy.data("tapListenerAdded", true);
                const node = e.target;
                const currentPath = window.location.pathname;
                dispatch(redirectToPost(accountName, node.id(), history));
              }
            });

            cy.on("mouseover", "node", function (e) {
              var sel = e.target;
              cy.elements()
                .difference(sel.outgoers())
                .not(sel)
                .addClass("semitransp");
              sel.addClass("highlight").outgoers().addClass("highlight");
            });

            cy.on("mouseout", "node", function (e) {
              var sel = e.target;
              cy.elements().removeClass("semitransp");
              sel.removeClass("highlight").outgoers().removeClass("highlight");
            });
            cyRef.current = cy;

            // cy.removeAllListeners();
          }}
          layout={layout}
          elements={elements}
          style={{ width: "100%", height: "100%" }}
          stylesheet={cytoscapeStylesheet}
          zoomingEnabled={true}
          userZoomingEnabled={true}
          minZoom={0.1}
          maxZoom={10}
          wheelSensitivity={0.1}
        />
        <div id="control">
          <div id="legend">
            <h3>Legend</h3>
            <ul>
              <li>
                <span
                  className="legend-color"
                  style={{
                    backgroundColor: theme == "dark" ? "purple" : "#90fc90",
                  }}
                ></span>{" "}
                Topics
              </li>
              <li>
                <span
                  className="legend-color"
                  style={{
                    backgroundColor: theme == "dark" ? "#1976d2" : "#9ee1f7",
                  }}
                ></span>{" "}
                User Posts
              </li>
            </ul>
          </div>
          <div className="h-5"></div>
          <Button
            type="button"
            theme="primary"
            text={"Centre Graph"}
            onClick={handlePanToOrigin}
          />
        </div>
      </div>
      <style>
        {`
        #cy-box {
          position: relative;
        }
        #control {
          position: absolute;
          color: black;
          top: 10px;
          left: 10px;
          padding: 10px;
          z-index: 10;
        }
        #legend{
          top: 10px;
          left: 10px;
          padding: 10px;
          z-index: 10;
          border: 1px solid black;
          background-color: white;
        }
        
        .legend-color {
          display: inline-block;
          width: 10px;
          height: 10px;
          margin-right: 5px;
        }
      `}
      </style>
    </>
  );
};

export default MyApp;
