import React, { useRef } from 'react';
import { Link } from 'react-router-dom';

import FeedCarousel from 'soapbox/features/feed-filtering/feed-carousel';
import LinkFooter from 'soapbox/features/ui/components/link-footer';
import {
  WhoToFollowPanel,
  TrendsPanel,
  SignUpPanel,
  PromoPanel,
  FundingPanel,
  CryptoDonatePanel,
  BirthdayPanel,
  CtaBanner,
  AnnouncementsPanel,
} from 'soapbox/features/ui/util/async-components';
import { useAppSelector, useOwnAccount, useFeatures, useSoapboxConfig } from 'soapbox/hooks';

// import { HashtagTimeline } from 'soapbox/features/ui/util/async-components';
import HashtagTimeline from 'soapbox/features/hashtag-timeline';
import { Avatar, Card, CardBody, HStack, Layout } from 'soapbox/components/ui';
import ComposeForm from 'soapbox/features/compose/components/compose-form';
import BundleContainer from 'soapbox/features/ui/containers/bundle-container';

interface IHomePage {
  children: React.ReactNode
}


function ProgressBar(props: { teamAscore: any; teamBscore: any; }) {
  const { teamAscore, teamBscore } = props
  console.log(teamAscore, teamBscore)
  const percentA = Math.round(teamAscore * 100 / (teamAscore + teamBscore))
  const percentB = Math.round(teamBscore * 100 / (teamAscore + teamBscore))
  console.log(percentA, percentB)
  return (
      <div className="w-full h-4 bg-gray-300 rounded-full overflow-hidden flex flex-row">
          <div className="h-full w-1/2 bg-white " style={{ backgroundImage: `linear-gradient(to left, red ${percentA}%, white ${percentA}%)` }}></div>
          <div className="h-full w-1/2 bg-white" style={{ backgroundImage: `linear-gradient(to right, blue ${percentB}%, white ${percentB}%)` }}></div>
      </div>
  )
}

function Statistic(props: { name: string | number | boolean | React.ReactElement<any, string | React.JSXElementConstructor<any>> | React.ReactFragment | React.ReactPortal | null | undefined; teamAscore: string | number | boolean | React.ReactElement<any, string | React.JSXElementConstructor<any>> | React.ReactFragment | React.ReactPortal | null | undefined; teamBscore: string | number | boolean | React.ReactElement<any, string | React.JSXElementConstructor<any>> | React.ReactFragment | React.ReactPortal | null | undefined; }) {
  return (
      <div className='flex flex-col items-center'>
          <p className='p-1'>{props.name}</p>
          <div className='flex flex-row items-end'>
              <span className='pr-4'>{props.teamAscore}</span>
              <span className='pl-4'>{props.teamBscore}</span>
          </div>
          <ProgressBar teamAscore={props.teamAscore} teamBscore={props.teamBscore}></ProgressBar>
      </div>
  )
}

function Scoreline() {
  return (
      <div className='flex flex-col items-center justify-center w-full'>
          <div className="flex flex-row items-center justify-center text-5xl font-bold ">
              <span className="text-red-500 mr-3">LFC</span>
              <span className="text-gray-500">7 - 0</span>
              <span className="text-blue-500 ml-3">MAN</span>
          </div>
          <div className='flex flex-row items-center justify-center text-2xl text-center'>
              <span>88:30</span>
          </div>
      </div>
  );
}
function Comments() {
  return (
      <div className="bg-white shadow rounded-lg m-3 px-4 py-2">
          <div className="flex flex-row">
              <div className="flex-shrink-0">
                  <img className="h-10 w-10 rounded-full" src="https://via.placeholder.com/150" alt="" />
              </div>
              <div className="ml-4">
                  <div className="text-sm font-medium text-gray-900">John Doe</div>
                  <div className="text-sm text-gray-500">Posted on April 1, 2023</div>
              </div>
          </div>
          <div className="mt-2 text-sm text-gray-700">
              This is a comment.
          </div>
      </div>
  )
}

function Layout1() {
  return (
      <div className="flex flex-col h-screen">
          <div className="flex flex-row flex-1">
              <div className="flex flex-col flex-1">
                  <div className="flex flex-row h-1/2 p-4">
                      <Scoreline></Scoreline>
                  </div>
                  <div className="max-h-72 overflow-y-scroll">
                  <HashtagTimeline params={{id:"manutd"}}></HashtagTimeline>
                  </div>
              </div>
              <div className="flex flex-col flex-1">
              <div className="h-1/2 p-4">
                        <Statistic name="Shots" teamAscore={12} teamBscore={6}></Statistic>
                        <Statistic name="On Target" teamAscore={9} teamBscore={1}></Statistic>
                        <Statistic name="Saves" teamAscore={5} teamBscore={6}></Statistic>
                        <Statistic name="Passes" teamAscore={320} teamBscore={104}></Statistic>
                        {/* <Statistic></Statistic>  */}
                    </div>
                  <div className="max-h-72 overflow-y-scroll">
                  <HashtagTimeline params={{id:"lfc"}}></HashtagTimeline>
                  </div>
              </div>
          </div>
      </div>

  )
}

const SportsPage: React.FC<IHomePage> = ({ children }) => {
  const me = useAppSelector(state => state.me);
  const account = useOwnAccount();
  const features = useFeatures();
  const soapboxConfig = useSoapboxConfig();

  const composeBlock = useRef<HTMLDivElement>(null);

  const hasPatron = soapboxConfig.extensions.getIn(['patron', 'enabled']) === true;
  const hasCrypto = typeof soapboxConfig.cryptoAddresses.getIn([0, 'ticker']) === 'string';
  const cryptoLimit = soapboxConfig.cryptoDonatePanel.get('limit', 0);

  const acct = account ? account.acct : '';
  const avatar = account ? account.avatar : '';

  return (
    <>
      <Layout.Main className='col-span-12'>
        <Layout1></Layout1>
      </Layout.Main>
    </>
  );
};

export default SportsPage;
